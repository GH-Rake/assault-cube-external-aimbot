#pragma once
#include <Windows.h>
#include <iostream>
#include <string>

class TrainerDisplay
{
public:
	std::string sGameStatus = "Game Not Found";
	std::string sAimbotStatus = "OFF";
	std::string sNoRecoilStatus = "OFF";
	std::string sAimModeStatus = "Angle";

	void SetWindow()
	{
		SetConsoleTitleA("Rake's External AssaultCube Aimbot");
		system("mode 53, 30");
		system("color 0a");
	}
	void Print()
	{
		system("cls");
		std::cout << "----------------------------------------------------\n"
			<< "----------------------------------------------------\n\n"
			<< "                 GuidedHacking.com                  \n"
			<< "     AnomanderRake's Assault Cube Aimbot v1.5       \n\n"
			<< "----------------------------------------------------\n"
			<< "----------------------------------------------------\n\n"
			<< "                 F5 to Start Aimbot                 \n"
			<< "   Aimbot will activate when you pull the trigger   \n\n"
			<< "----------------------------------------------------\n"
			<< "----------------------------------------------------\n\n"
			<< "GAME STATUS:           " << sGameStatus << "   \n\n"
			<< "[F5] Aimbot             -> " << sAimbotStatus << " <-\n\n"
			<< "[F6] Aimbot Mode        -> " << sAimModeStatus << " <-\n\n"
			<< "[F7] No Recoil          -> " << sNoRecoilStatus << " <-\n\n"
			<< "[INSERT] Exit\n\n"
			<< "----------------------------------------------------\n"
			<< "----------------------------------------------------\n";
	}

	void Print(std::string *StatusType, LPCSTR Status)
	{
		if (*StatusType != Status)
		{
			*StatusType = Status;
			Print();
		}
	}

}Display;