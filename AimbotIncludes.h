//GuidedHacking.com
#include <Windows.h>
#include <tlhelp32.h>
#include <tchar.h>
#include "geom.h"
#include "menu.h"
#define WeaponSemiAuto (t_WeaponNum == 1 || t_WeaponNum == 2 || t_WeaponNum == 3 || t_WeaponNum == 5 || t_WeaponNum == 7 || t_WeaponNum == 0 || t_WeaponNum == 8)

//Globals ooooh scary!!
DWORD dwProcID = NULL;
HANDLE hProcHandle = NULL;

DWORD GetProcessID(const wchar_t * ExeName) {
	PROCESSENTRY32 ProcEntry = { 0 };
	HANDLE SnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (!SnapShot)
		return NULL;

	ProcEntry.dwSize = sizeof(ProcEntry);

	if (!Process32First(SnapShot, &ProcEntry))
		return NULL;

	do {
		if (!wcscmp(ProcEntry.szExeFile, ExeName)) {
			CloseHandle(SnapShot);
			return ProcEntry.th32ProcessID;
		}
	} while (Process32Next(SnapShot, &ProcEntry));

	CloseHandle(SnapShot);
	return NULL;
}

class HackClass
{
public:
	HWND gameWindow = NULL;
	int KeyPressTimer = 0;
	int UpdateAddressesTimer = 0;
	bool bUpdatedOnce = false;
	bool bGetProcessDataCurrent = false;
	uintptr_t ac_clientModule = 0;
	uintptr_t botClassAddr = 0;
	uintptr_t humanClassAddr = 0;
	uintptr_t localPlayerAddr = 0;
	uintptr_t playerArrayPointer = 0;
	uintptr_t playerArrayAddress = 0;

	void GetProcess()
	{
		dwProcID = GetProcessID(_T("ac_client.exe"));
		if (dwProcID)
		{
			hProcHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcID);
			if (hProcHandle == INVALID_HANDLE_VALUE || !hProcHandle)
			{
				Display.Print(&Display.sGameStatus, "Game Not Found");
			}
			else
			{
				Display.Print(&Display.sGameStatus, "Game Found");
				bGetProcessDataCurrent = true;
			}
		}

		else
		{
			Display.Print(&Display.sGameStatus, "Game Not Found");
		}
	}

	uintptr_t dwGetModuleBaseAddress(DWORD dwProcID, TCHAR *szModuleName)
	{
		uintptr_t dwModuleBaseAddress = 0;
		HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, dwProcID);
		if (hSnapshot != INVALID_HANDLE_VALUE)
		{
			MODULEENTRY32 ModuleEntry32;
			ModuleEntry32.dwSize = sizeof(MODULEENTRY32);
			if (Module32First(hSnapshot, &ModuleEntry32))
			{
				do
				{
					if (_tcsicmp(ModuleEntry32.szModule, szModuleName) == 0)
					{
						dwModuleBaseAddress = (uintptr_t)ModuleEntry32.modBaseAddr;
						break;
					}
				} while (Module32Next(hSnapshot, &ModuleEntry32));
			}
			CloseHandle(hSnapshot);
		}
		return dwModuleBaseAddress;
	}

	uintptr_t CalculatePointer(HANDLE hProcHandle, int PointerLevel, uintptr_t Offsets[], uintptr_t BaseAddress)
	{
		uintptr_t Pointer = BaseAddress;
		uintptr_t TempBuffer;

		uintptr_t PointerAddress;
		for (int i = 0; i < PointerLevel; i++)
		{
			if (i == 0)
			{
				ReadProcessMemory(hProcHandle, (LPCVOID)Pointer, &TempBuffer, 4, NULL);
			}

			PointerAddress = TempBuffer + Offsets[i];
			ReadProcessMemory(hProcHandle, (LPCVOID)PointerAddress, &TempBuffer, 4, NULL);
		}
		return PointerAddress;
	}

	void UpdateAddresses()
	{
		ac_clientModule = dwGetModuleBaseAddress(dwProcID, _T("ac_client.exe"));
		ReadProcessMemory(hProcHandle, (LPCVOID)(ac_clientModule + 0x10F4F4), &localPlayerAddr, 4, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(ac_clientModule + 0x10F4F8), &playerArrayAddress, 4, NULL);
		botClassAddr = ac_clientModule + 0xe4ac0;//
		humanClassAddr = ac_clientModule + 0xE4A98;//
		Hack.bUpdatedOnce = true;
	}
}Hack;

class CurrentGameData
{
public:
	int GameMode;
	int NumOfPlayers = 0;
	bool bTeamGame = false;

	void UpdateCurrentGameData()
	{
		ReadProcessMemory(hProcHandle, (LPCVOID)(Hack.ac_clientModule + 0x10F500), &NumOfPlayers, 4, NULL);

		if (NumOfPlayers != 0)
		{
			bTeamGame = false;
			ReadProcessMemory(hProcHandle, (LPCVOID)(0x50F49C), &GameMode, 1, NULL);
			int m_teamMode[11] = { 0, 4, 5, 7, 13, 11, 14, 17, 16, 20, 21 };
			for each(int i in m_teamMode)
			{
				if (i == GameMode)
				{
					bTeamGame = true;
					break;
				}
			}
		}
	}

}GameData;

class playerClass
{
public:
	uintptr_t PlayerAddress;
	char Name[16];
	BYTE Team;
	int Health;
	BYTE State;
	vec3 position;
	vec3 angles;
	vec3 aimbotAngle;
	float Distance;
	float angleCrossToEnemy;
	bool bWeaponSemiAuto = 0;

	//default constructor for local player
	playerClass()
	{
		PlayerAddress = Hack.localPlayerAddr;
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x225), &Name, 16, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x32c), &Team, 1, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0xF8), &Health, 4, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x34), &position, 12, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x338), &State, 1, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x40), &angles, 12, NULL);//

		int t_WeaponNum;
		uintptr_t tmpOffsets[2] = { 0x374, 0x4 };
		ReadProcessMemory(hProcHandle, (LPCVOID)(Hack.CalculatePointer(hProcHandle, 2, tmpOffsets, 0x509B74)), &t_WeaponNum, 4, NULL);

		if (WeaponSemiAuto)
		{
			bWeaponSemiAuto = true;
		}
		else
		{
			bWeaponSemiAuto = false;
		}
	}

	//Constructor for other players
	playerClass(uintptr_t playerAddr)
	{
		PlayerAddress = playerAddr;
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x225), &Name, 16, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x32c), &Team, 1, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0xF8), &Health, 4, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x34), &position, 12, NULL);
		ReadProcessMemory(hProcHandle, (LPCVOID)(PlayerAddress + 0x338), &State, 1, NULL);
	}

}localPlayer;