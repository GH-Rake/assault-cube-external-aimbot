//GuidedHacking.com
#include "AimbotIncludes.h"
#include <vector>
std::vector<playerClass> playerVector;
bool playerSorter(playerClass &lhs, playerClass &rhs);

class Aimbot
{
public:
	bool bAimbot = false;
	bool bNoRecoilStatus = false;
	uintptr_t playerAddr[31];
	bool bSortByAngle = true;

	void sortPlayerVector()
	{
		for (auto&& player : playerVector)
		{
			player.aimbotAngle = CalcAngle(localPlayer.position, player.position);

			if (bSortByAngle)
			{
				player.angleCrossToEnemy = DifferenceOfAngles(player.aimbotAngle, localPlayer.angles);
			}
			else
			{
				player.Distance = Distance(player.position, localPlayer.position);
			}
		}
		sort(playerVector.begin(), playerVector.end(), playerSorter);
	}

	void aim()
	{
		//automatic weapon functionality
		if (localPlayer.bWeaponSemiAuto == false)
		{
			WriteProcessMemory(hProcHandle, (PBYTE*)(localPlayer.PlayerAddress + 0x40), &playerVector[0].aimbotAngle, 8, NULL);
		}

		//semi auto weapons aim first, then shoot
		else
		{
			WriteProcessMemory(hProcHandle, (PBYTE*)(localPlayer.PlayerAddress + 0x40), &playerVector[0].aimbotAngle, 8, NULL);
			//sets bFiring true
			WriteProcessMemory(hProcHandle, (PBYTE*)(localPlayer.PlayerAddress + 0x224), "x01", 1, NULL);
		}
	}

	void ReadHotKeys()
	{
		while (GetAsyncKeyState(VK_LBUTTON) && aBot.bAimbot)
		{
			aBot.aim();
			readPlayerData();
			sortPlayerVector();
		}

		if (GetAsyncKeyState(VK_F5) &1)
		{
			aBot.bAimbot = !aBot.bAimbot;
			if (aBot.bAimbot)
			{
				Display.Print(&Display.sAimbotStatus, "ON");
			}
			else
			{
				Display.Print(&Display.sAimbotStatus, "OFF");
			}
		}

		if (GetAsyncKeyState(VK_F6) &1)
		{
			aBot.bSortByAngle = !aBot.bSortByAngle;
			if (aBot.bSortByAngle)
			{
				Display.Print(&Display.sAimModeStatus, "Angle");
			}
			else
			{
				Display.Print(&Display.sAimModeStatus, "Distance");
			}
		}

		if (GetAsyncKeyState(VK_F7) &1)
		{
			aBot.bNoRecoilStatus = !aBot.bNoRecoilStatus;
			if (aBot.bNoRecoilStatus)
			{
				WriteProcessMemory(hProcHandle, (LPVOID)(0x463786), "\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90", 10, NULL);
				Display.Print(&Display.sNoRecoilStatus, "ON");
			}
			else
			{
				WriteProcessMemory(hProcHandle, (LPVOID)(0x463786), "\x50\x8D\x4C\x24\x1C\x51\x8B\xCE\xFF\xD2", 10, NULL);
				Display.Print(&Display.sNoRecoilStatus, "OFF");
			}
		}
	}

	void readPlayerData()
	{
		localPlayer = playerClass();
		playerVector.clear();
		ReadProcessMemory(hProcHandle, (LPCVOID)Hack.playerArrayAddress, &playerAddr, 124, NULL);
		for each(uintptr_t player in playerAddr)
		{
			uintptr_t tmp;
			if (ReadProcessMemory(hProcHandle, (LPCVOID)player, &tmp, 4, NULL) == TRUE)
			{
				if (tmp == Hack.humanClassAddr || tmp == Hack.botClassAddr)
				{
					playerVector.push_back(playerClass(player));
				}
			}
		}
	}
}aBot;

bool playerSorter(playerClass &lhs, playerClass &rhs)
{
	//if not alive, move to end
	if (lhs.State != 0)
		return false;

	else
	{
		if (GameData.bTeamGame)
		{
			//if lhs is not on my team, but rhs is on my team then sort out rhs
			if (lhs.Team != localPlayer.Team && rhs.Team == localPlayer.Team)
			{
				return true;
			}

			//if lhs is on my team, but rhs is not sort out lhs
			if (lhs.Team == localPlayer.Team && rhs.Team != localPlayer.Team)
			{
				return false;
			}

			
			else
			{
				if (aBot.bSortByAngle)
				{
					return lhs.angleCrossToEnemy < rhs.angleCrossToEnemy;
					//return false;
				}
				else
				{
					return lhs.Distance < rhs.Distance;
				}
			}
			
		}

		else //for death match games
		{
			if (aBot.bSortByAngle)
			{
				return lhs.angleCrossToEnemy < rhs.angleCrossToEnemy;
			}
			else
			{
				return lhs.Distance < rhs.Distance;
			}
		}
	}
}

int main()
{
	//TrainerDisplay Display;


	Display.SetWindow();
	Display.Print();

	while (!GetAsyncKeyState(VK_INSERT))
	{
		Hack.GetProcess();
		Hack.UpdateAddresses();

		GameData.UpdateCurrentGameData();
		aBot.ReadHotKeys();

		if (aBot.bAimbot && GameData.NumOfPlayers != 0)
		{
			aBot.readPlayerData();
			aBot.sortPlayerVector();
		}
	}
	CloseHandle(hProcHandle);
	return ERROR_SUCCESS;
}