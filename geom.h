#pragma once
#include <algorithm>
#define PI 3.1415927f
#define DegToRad(A) ( A * 180.0f / PI )
#define ACUBE(A) ( A / PI * 180.0f + 180.0f )

struct vec3
{
	union
	{
		struct { float x, y, z; };
		float v[3];
	};
};

vec3 Subtract(vec3 src, vec3 dst);

float Magnitude(vec3 vec);

float Distance(vec3 src, vec3 dst);

vec3 Normalize(vec3 src);

vec3 CalcAngle(vec3 src, vec3 dst);

float DifferenceOfAngles(vec3 src, vec3 dst);