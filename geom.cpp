#include "geom.h"

vec3 Add(vec3 src, vec3 dst)
{
	vec3 sum;
	sum.x = src.x + dst.x;
	sum.y = src.y + dst.y;
	sum.z = src.z + dst.z;
	return sum;
}

vec3 Subtract(vec3 src, vec3 dst)
{
	vec3 diff;
	diff.x = src.x - dst.x;
	diff.y = src.y - dst.y;
	diff.z = src.z - dst.z;
	return diff;
}

vec3 Divide(vec3 src, float num)
{
	vec3 vec;
	vec.x = src.x / num;
	vec.y = src.y / num;
	vec.z = src.z / num;

	return vec;
}

float DotProduct(vec3 src, vec3 dst)
{
	return src.x * dst.x + src.y * dst.y + src.z * dst.z;
}

float Magnitude(vec3 vec)
{
	return sqrtf(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
}

float Distance(vec3 src, vec3 dst)
{
	vec3 diff = Subtract(src, dst);
	return Magnitude(diff);
}

vec3 Normalize(vec3 src)
{
	vec3 vec = Divide(src, Magnitude(src));
	return vec;
}

vec3 CalcAngle(vec3 src, vec3 dst)
{
	vec3 angles;
	angles.x = ACUBE(-(float)atan2(dst.x - src.x, dst.y - src.y));
	angles.y = DegToRad(atan2(dst.z - src.z, Distance(src, dst)));
	angles.z = 0.0f;
	return angles;
}


float DifferenceOfAngles(vec3 src, vec3 dst)
{
	vec3 diff;

	diff.y = src.y - dst.y;
	diff.x = src.x - dst.x;

	//normalize by making them positive values if they are negative
	if (diff.y < 0)
	{
		diff.y *= -1;
	}
	if (diff.x < 0)
	{
		diff.x *= -1;
	}

	//add them together and divide by 2, gives an average of the 2 angles
	float fDifference = (diff.y + diff.x) / 2;
	return fDifference;
}

/*
//new shit DOESNT WORK!
float DifferenceOfAngles(vec3 src, vec3 dst)
{
	float dot = DotProduct(src, dst);
	return dot / (Magnitude(src) * Magnitude(dst));
}
*/

vec3 NormalizeA(vec3 angle)
{
	for (auto a : angle.v)
	{
		if (a > 180.f)
		{
			a -= 360.f;
		}

		if ( a < -180.f)
		{
			a += 360.f;
		}
	}
	return angle;
}

/* kinda works from mambda
float DifferenceOfAngles(vec3 src, vec3 dst)
{
float dot = DotProduct(src, dst);
// formula for dot product is |a| * |b| * cos( theta ), so given we have dot, we can work backwards for theta.
// dot = |a| * |b| * cos( t )
// dot = lens * cos( t )
// dot / lens = cos( t )
// t = cos-1( dot / lens )
//float mag = src.length() * dst.length();


float mag = Magnitude(src) * Magnitude(dst);
float diff = acosf(dot / mag);
return diff;
}

*/